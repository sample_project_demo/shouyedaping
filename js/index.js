// 柱状图模块1
(function () {
  // 实例化对象
  let myChart = echarts.init(document.querySelector(".bar .chart"));
  // 指定配置项和数据
  let option = {
    color: ["#2f89cf"],
    tooltip: {
      trigger: "axis",
      axisPointer: {
        // 坐标轴指示器，坐标轴触发有效
        type: "shadow", // 默认为直线，可选为：'line' | 'shadow'
      },
    },
    grid: {
      left: "0%",
      top: "10px",
      right: "0%",
      bottom: "4%",
      // 是否为刻度标签保留位置
      containLabel: true,
    },
    xAxis: [
      {
        type: "category",
        data: [
          "旅游行业",
          "教育培训",
          "游戏行业",
          "医疗行业",
          "社交行业",
          "电商行业",
          "金融行业",
        ],
        axisTick: {
          alignWithLabel: true,
        },
        // 修改刻度标签相关样式
        axisLabel: {
          color: "rgba(255, 255, 255, .6)",
          fontSize: "12",
        },
        // 不显示x坐标轴样式
        axisLine: {
          show: false,
        },
      },
    ],
    yAxis: [
      {
        type: "value",
        // 修改刻度标签相关样式
        axisLabel: {
          color: "rgba(255, 255, 255, .6)",
          fontSize: "12",
        },
        // y轴的线条相关样式
        axisLine: {
          lineStyle: {
            color: "rgba(255, 255, 255, .1)",
            width: 1,
          },
        },
        // y轴分割线的颜色
        splitLine: {
          lineStyle: {
            color: "rgba(255, 255, 255, .1)",
          },
        },
      },
    ],
    series: [
      {
        name: "直接访问",
        type: "bar",
        // 修改柱子的宽度
        barWidth: "35%",
        data: [10, 52, 200, 334, 390, 330, 220],
        itemStyle: {
          barBorderRadius: 5,
        },
      },
    ],
  };
  // 把配置项给实例对象
  myChart.setOption(option);
  window.onresize = function () {
    myChart.resize();
  };
})();

// 柱状图模块2
(function () {
  // 实例化对象
  let myChart = echarts.init(document.querySelector(".bar2 .chart"));
  let myColor = ["#1089E7", "#F57474", "#56D0E3", "#F8B448", "#8B78F6"];
  option = {
    // 图表的位置
    grid: {
      top: "10%",
      left: "22%",
      bottom: "10%",
    },
    // 隐藏x轴相关信息
    xAxis: {
      show: false,
    },
    yAxis: [
      {
        show: true,
        data: ["HTML5", "CSS3", "javascript", "VUE", "NODE"],
        inverse: true,
        // 是否显示轴线
        axisLine: {
          show: false,
        },
        // 是否显示分割线
        splitLine: {
          show: false,
        },
        // 是否显示刻度
        axisTick: {
          show: false,
        },
        // 修改刻度标签相关样式
        axisLabel: {
          color: "#fff",
          fontSize: 12,
        },
      },
      {
        show: true,
        inverse: true,
        data: [702, 350, 610, 793, 664],
        axisTick: {
          show: false,
        },
        axisLine: {
          show: false,
        },
        axisLabel: {
          fontSize: 12,
          color: "#fff",
        },
      },
    ],
    series: [
      {
        name: "条",
        type: "bar",
        // 类似z-index
        yAxisIndex: 0,
        data: [70, 34, 60, 78, 69],
        barCategoryGap: 50,
        barWidth: 10,
        itemStyle: {
          normal: {
            barBorderRadius: 20,
            color: function (params) {
              var num = myColor.length;
              return myColor[params.dataIndex % num];
            },
          },
        },
        label: {
          normal: {
            show: true,
            position: "inside",
            formatter: "{c}%",
          },
        },
      },
      {
        name: "框",
        type: "bar",
        yAxisIndex: 1,
        barCategoryGap: 50,
        data: [100, 100, 100, 100, 100],
        barWidth: 15,
        itemStyle: {
          normal: {
            color: "none",
            borderColor: "#00c1de",
            borderWidth: 3,
            barBorderRadius: 15,
          },
        },
      },
    ],
  };

  // 使用刚指定的配置项和数据显示图表。
  myChart.setOption(option);
  window.addEventListener("resize", function () {
    myChart.resize();
  });
})();

// 折线图模块1
(function () {
  // 实例化对象
  let myChart = echarts.init(document.querySelector(".line .chart"));
  // 设置配置项
  let option = {
    color: ["#00f2f1", "#ed3f35"],
    tooltip: {
      trigger: "axis",
    },
    legend: {
      // 如果series 里面有name,legend里面的data可以不要
      // data: ['前端新增', '后端新增'],
      right: "10%",
      textStyle: {
        color: "#4c9bfd",
      },
    },
    grid: {
      top: "20%",
      left: "3%",
      right: "4%",
      bottom: "3%",
      // 显示边框
      show: true,
      // 边框颜色
      borderColor: "#012f4a",
      containLabel: true,
    },
    xAxis: {
      type: "category",
      boundaryGap: false,
      data: ["1月", "3月", "5月", "7月", "9月", "11月"],
      // 去掉刻度线
      axisTick: {
        show: false,
      },
      // 刻度标签
      axisLabel: {
        color: "#4c9bfd",
      },
      // 去掉轴线
      axisLine: {
        show: false,
      },
    },
    yAxis: {
      type: "value",
      // 去掉刻度线
      axisTick: {
        show: false,
      },
      // 刻度标签
      axisLabel: {
        color: "#4c9bfd",
      },
      // 去掉轴线
      axisLine: {
        show: false,
      },
      // 分割线
      splitLine: {
        lineStyle: {
          color: "#012f4a",
        },
      },
    },
    series: [
      {
        name: "前端新增",
        type: "line",
        // 是否堆叠
        // stack: "总量",
        // 是否平滑显示曲线
        smooth: true,
        data: [120, 132, 101, 134, 90, 230, 210],
      },
      {
        name: "后端新增",
        type: "line",
        // stack: "总量",
        smooth: true,
        data: [220, 182, 191, 234, 290, 330, 310],
      },
    ],
  };
  // 将配置给实例对象
  myChart.setOption(option);
  // 监听屏幕大小变化
  window.onresize = function () {
    myChart.resize();
  };
})();

// 折线图模块2
(function () {
  // 实例化
  let myChart = echarts.init(document.querySelector(".line2 .chart"));
  // 配置项
  let option = {
    tooltip: {
      trigger: "axis",
      // 鼠标移上去的虚线
      // axisPointer: {
      //   type: "cross",
      //   label: {
      //     backgroundColor: "#6a7985",
      //   },
      // },
    },
    legend: {
      // data: ["前端", "后端"],
      top: "0%",
      textStyle: {
        color: "rgba(255, 255, 255, .5)",
        fontSize: "12",
      },
    },
    grid: {
      top: "30",
      left: "10",
      right: "10",
      bottom: "10",
      containLabel: true,
    },
    xAxis: [
      {
        type: "category",
        boundaryGap: false,
        data: [
          "01",
          "02",
          "03",
          "04",
          "05",
          "06",
          "07",
          "08",
          "09",
          "11",
          "12",
          "13",
          "14",
          "15",
          "16",
          "17",
          "18",
          "19",
          "20",
          "21",
          "22",
          "23",
          "24",
          "25",
          "26",
          "27",
          "28",
          "29",
          "30",
        ],
        // 刻度标签
        axisLabel: {
          textStyle: {
            color: "rgba(255, 255, 255, .6)",
            fontSize: "12",
          },
        },
        // 轴线
        axisLine: {
          lineStyle: {
            color: "rgba(255, 255, 255, .2)",
          },
        },
      },
    ],
    yAxis: [
      {
        type: "value",
        axisLabel: {
          textStyle: {
            color: "rgba(255, 255, 255, .6)",
            fontSize: "12",
          },
        },
        // 刻度
        axisTick: {
          show: false,
        },
        // 轴线
        axisLine: {
          lineStyle: {
            color: "rgba(255, 255, 255, .2)",
          },
        },
        // 分割线
        splitLine: {
          lineStyle: {
            color: "rgba(255, 255, 255, .1)",
          },
        },
      },
    ],
    series: [
      {
        name: "前端",
        type: "line",
        smooth: true,
        // 填充区域设置
        areaStyle: {
          color: new echarts.graphic.LinearGradient(
            0,
            0,
            0,
            1,
            [
              {
                offset: 0,
                color: "rgba(1, 132, 213, 0.4)",
              },
              {
                offset: 0.8,
                color: "rgba(1, 132, 213, 0.1)",
              },
            ],
            false
          ),
          shadowColor: "rgba(0, 0, 0, 0.1)",
        },
        // 拐点设置
        symbol: "circle",
        // 拐点大小
        symbolSize: 5,
        // 开始不显示拐点，鼠标经过才显示
        showSymbol: false,
        // 设置拐点的颜色以及边框
        itemStyle: {
          color: "#0184d5",
          borderColor: "rgba(221, 220, 107, .1)",
          borderWidth: 12,
        },
        // 单独修改当前线条的样式
        lineStyle: {
          color: "#0184d5",
          width: "2",
        },
        data: [
          30,
          40,
          30,
          40,
          30,
          40,
          30,
          60,
          20,
          40,
          20,
          40,
          30,
          40,
          30,
          40,
          30,
          40,
          30,
          60,
          20,
          40,
          20,
          40,
          30,
          60,
          20,
          40,
          20,
          40,
        ],
      },
      {
        name: "后端",
        type: "line",
        smooth: true,
        symbol: "circle",
        symbolSize: 5,
        showSymbol: false,
        lineStyle: {
          normal: {
            color: "#00d887",
            width: 2,
          },
        },
        areaStyle: {
          normal: {
            color: new echarts.graphic.LinearGradient(
              0,
              0,
              0,
              1,
              [
                {
                  offset: 0,
                  color: "rgba(0, 216, 135, 0.4)",
                },
                {
                  offset: 0.8,
                  color: "rgba(0, 216, 135, 0.1)",
                },
              ],
              false
            ),
            shadowColor: "rgba(0, 0, 0, 0.1)",
          },
        },
        itemStyle: {
          normal: {
            color: "#00d887",
            borderColor: "rgba(221, 220, 107, .1)",
            borderWidth: 12,
          },
        },
        data: [
          50,
          30,
          50,
          60,
          10,
          50,
          30,
          50,
          60,
          40,
          60,
          40,
          80,
          30,
          50,
          60,
          10,
          50,
          30,
          70,
          20,
          50,
          10,
          40,
          50,
          30,
          70,
          20,
          50,
          10,
          40,
        ],
      },
    ],
  };
  // 把配置项给实例
  myChart.setOption(option);
  window.onresize = function () {
    myChart.resize();
  };
})();

// 饼状图模块1
(function () {
  // 实例化
  let myChart = echarts.init(document.querySelector(".pie .chart"));
  // 配置项
  let option = {
    color: ["#065aab", "#066eab", "#0682ab", "#0696ab", "#06a0ab"],
    tooltip: {
      trigger: "item",
      formatter: "{a} <br/>{b}: {c} ({d}%)",
    },
    legend: {
      // orient: "vertical",
      bottom: "0%",
      data: ["0岁以下", "20-29岁", "30-39岁", "40-49岁", "50岁以上"],
      // 图例宽高
      itemWidth: 10,
      itemHeight: 10,
      textStyle: {
        color: "rgba(255, 255, 255, .5)",
        fontSize: "12",
      },
    },
    series: [
      {
        name: "年龄分布",
        type: "pie",
        // 饼形图的大小（内圆半径 外圆半径）
        radius: ["40%", "60%"],
        avoidLabelOverlap: false,
        // 是否显示文字标签（圈内）
        label: {
          show: false,
          // position: "center",
        },
        // 圈内提示文字
        // emphasis: {
        //   label: {
        //     show: true,
        //     fontSize: "30",
        //     fontWeight: "bold",
        //   },
        // },
        // 链接图形和文字的线是否显示
        labelLine: {
          show: true,
        },
        data: [
          { value: 1, name: "0岁以下" },
          { value: 4, name: "20-29岁" },
          { value: 2, name: "30-39岁" },
          { value: 2, name: "40-49岁" },
          { value: 1, name: "50岁以上" },
        ],
      },
    ],
  };
  myChart.setOption(option);
  window.onresize = function () {
    myChart.resize();
  };
})();

// 饼状图模块2
-(function () {
  let myChart = echarts.init(document.querySelector(".pie2 .chart"));
  let option = {
    color: ["#006cff", "#60cda0", "#ed8884", "#ff9f7f", "#0096ff", "#9fe6b8", "#32c5e9", "#1d9dff"],
    tooltip: {
      trigger: "item",
      formatter: "{a} <br/>{b} : {c} ({d}%)",
    },
    legend: {
      itemWidth: 10,
      itemHeight: 10,
      bottom: "0%",
      textStyle: {
        color: "rgba(255, 255, 255, .5)",
        fontSize: "12",
      },
      data: ["云南","北京","山东","河北","江苏","四川","杭州"]
    },
    series: [
      {
        name: "地区分布",
        type: "pie",
        radius: ["10%", "70%"],
        center: ["50%", "50%"],
        // 模式（半径模式 面积模式）
        roseType: "radius",
        label: {
          fontSize: 10,
        },
        // 链接图形和文字的线条
        labelLine: {
          // 链接图形的线条
          length: 6,
          // 链接文字的线条
          length2: 8
        },
        data: [
          { value: 20, name: "云南" },
          { value: 26, name: "北京" },
          { value: 24, name: "山东" },
          { value: 25, name: "河北" },
          { value: 20, name: "江苏" },
          { value: 30, name: "四川" },
          { value: 42, name: "杭州" }
        ],
      },
    ],
  };
  myChart.setOption(option)
  window.onresize = function() {
    myChart.resize()
  }
})();
